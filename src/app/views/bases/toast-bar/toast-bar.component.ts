import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-toast-bar',
  templateUrl: './toast-bar.component.html',
  styleUrls: ['./toast-bar.component.scss'],
})
export class ToastBarComponent implements OnInit {
  @Input() visible = false;
  @Input() color!: string;
  @Input() message = 'Thao tác thành công';
  position = 'top-end';
  constructor() {}

  ngOnInit(): void {}

  onVisibleChange(event: any) {
    this.visible = event;
  }
}
