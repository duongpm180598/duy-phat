import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ACTION } from 'src/app/constants/common.const';
import { ListProductResponse } from 'src/app/models/product.model';
import { ProductService } from 'src/app/services/product.service';
import { Util } from 'src/app/utils/util.helper';

@Component({
  selector: 'app-ticket',
  templateUrl: './ticket.component.html',
  styleUrls: ['./ticket.component.scss'],
})
export class TicketComponent implements OnInit {
  ACTION = ACTION;
  loading = false;
  filterForm: FormGroup;
  listProduct: ListProductResponse = {
    data: [],
    page: 1,
    pageSize: 1,
    total: 1,
    totalPage: 1,
  };
  constructor(
    private productService: ProductService,
    private formBuilder: FormBuilder,
    public util: Util
  ) {
    this.filterForm = this.formBuilder.group({
      search: [null, Validators.required],
    });
  }

  ngOnInit(): void {}

  submit() {
    // if (this.filterForm.valid) {
    // this.productService
    //   .getProductByBarcode(this.filterForm.get('search')?.value)
    //   .subscribe((data) => {
    //     this.listProduct = data;
    //   });
    this.listProduct.data.push({
      barcode: '8172383123',
      exportPrice: 293783,
      name: 'name',
    });
    // }
  }

  onChangePage(event: number) {
    // this.params.page = event;
    // this.getProducts();
  }

  handleAction(action: string) {}
}
