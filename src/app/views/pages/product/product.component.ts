import { Component, OnInit } from '@angular/core';
import { RequestParam } from 'src/app/models/common.model';
import { ListProductResponse, Product } from 'src/app/models/product.model';
import { ProductService } from '../../../services/product.service';
import { ACTION } from '../../../constants/common.const';
import { Util } from '../../../utils/util.helper';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.scss'],
})
export class ProductComponent implements OnInit {
  ACTION = ACTION;
  loading = false;
  params: RequestParam = {
    page: 1,
    pageSize: 25,
  };
  listProduct: ListProductResponse = {
    data: [],
    page: 1,
    pageSize: 1,
    total: 1,
    totalPage: 1,
  };
  product!: Product;
  visible = false;
  action = ACTION.CREATE;
  filterForm: FormGroup;

  constructor(
    private productService: ProductService,
    public util: Util,
    private formBuilder: FormBuilder
  ) {
    this.filterForm = this.formBuilder.group({
      search: [null, Validators.required],
    });
  }

  ngOnInit(): void {
    this.getProducts();
  }

  getProducts() {
    this.loading = true;
    this.productService.getProducts(this.params).subscribe(
      (data) => {
        this.listProduct = data;
        this.loading = false;
      },
      (error) => {
        this.loading = false;
      }
    );
  }

  handleAction(action: string, product?: any) {
    this.product = product;
    if (action === ACTION.CREATE) {
      this.product = {};
    }
    this.visible = !this.visible;
    this.action = action;
  }

  submit() {
    if (this.filterForm.valid) {
      this.productService
        .getProductByBarcode(this.filterForm.get('search')?.value)
        .subscribe((data) => {
          this.listProduct = data;
        });
    }
  }

  onActionChange(event: any) {
    this.visible = event;
  }

  onChangePage(event: number) {
    this.params.page = event;
    this.getProducts();
  }
}
